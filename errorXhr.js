// # errorXhr
//
// ###_Helps you processing error responses._
//
// ___
//
// **Author:** AB Zainuddin
//
// **Email:** burhan@codeyellow.nl
//
// **Website:** http://www.codeyellow.nl
//
// **Copyright** Copyright (c) 2013 Code Yellow B.V.
//
// **License:** Distributed under MIT license.
// ___
define(function (require) {
    return {
        /**
         * Parse xhr body according to content-type.
         *
         * @param {Object} fields All fields to invalidate.
         */
        parse: function (xhr) {
            // TODO: more than only json.
            // Parse json response body on error simulating xhr2 W3C spec
            if (xhr && xhr.getResponseHeader && xhr.getResponseHeader('content-type') && xhr.getResponseHeader('content-type').match(/^(application\/json|text\/javascript)/)) {
                var response = null;
                try {
                    response = xhr.response || (Backbone.$.parseJSON || JSON.parse)(xhr.responseText);
                } catch (e) {
                }
                xhr.response = response;
            }
            
            return xhr;
        },
        /**
         * Add invalid class for validation errors.
         *
         * @param {Marionette.View} view The view to invalidate fields.
         * @param {Object} fields All fields to invalidate.
         */
        invalidate: function (view, fields) {
            _.each(fields, function(errorCodes, field){
                view.$('[name="' + field + '"]').addClass('invalid');
            });
        },
        /**
         * Remove invalid class.
         *
         * @param {Marionette.View} view The view to invalidate fields.
         */
        unInvalidate: function (view) {            
            view.$('.invalid').removeClass('invalid');
        }
    };
});
