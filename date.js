define({    
    // # Constants
    SEC_IN_DAY: 86400,

    // # Functions    

    // Retrieve the spanning week.
    getWeek: function(date) {
        var monday = this.getMonday(date),
        sunday = this.getSunday(date);

        // Adjust times.
        monday.setHours(0);
        monday.setMinutes(0);
        monday.setSeconds(0);
        sunday.setHours(24);
        sunday.setMinutes(0);
        sunday.setSeconds(0);

        return [monday, sunday];
    },
    /* For a given date, get the ISO week number
     *
     * Based on information at:
     *
     *    http://www.merlyn.demon.co.uk/weekcalc.htm#WNR
     *
     * Algorithm is to find nearest thursday, it's year
     * is the year of the week number. Then get weeks
     * between that date and the first day of that year.
     *
     * Note that dates in one year can be weeks of previous
     * or next year, overlap is up to 3 days.
     *
     * e.g. 2014/12/29 is Monday in week  1 of 2015
     *      2012/1/1   is Sunday in week 52 of 2011
     */
    getWeekNumber: function(d) {
        // Copy date so don't modify original
        d = new Date(d);
        d.setHours(0,0,0);
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setDate(d.getDate() + 4 - (d.getDay()||7));
        // Get first day of year
        var yearStart = new Date(d.getFullYear(),0,1);
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7)
        // Return array of year and week number
        return [d.getFullYear(), weekNo];
    },
    getUnix: function(date) {
        return Math.round(date.getTime() / 1000);
    },
    getMonday: function(date) {
        var d = new Date(date);
        var day = d.getDay(), diff = d.getDate() - day + (day === 0 ? -6 : 1); // adjust

        return new Date(d.setDate(diff));
    },
    getSunday: function(date) {
        var d = new Date(date);
        var day = d.getDay(), diff = d.getDate() - day + (day === 0 ? 0 : 7); // adjust

        return new Date(d.setDate(diff));
    }
});
