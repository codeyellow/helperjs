// # object
//
// ###_ Helps you with objects._
//
// ___
//
// **Author:** AB Zainuddin
//
// **Email:** burhan@codeyellow.nl
//
// **Website:** http://www.codeyellow.nl
//
// **Copyright** Copyright (c) 2013 Code Yellow B.V.
//
// **License:** Distributed under MIT license.
// ___
define(function (require){
    var _ = require('underscore');

    return {
        unflatten: function(data) {
            var result = _.clone(data),
            ref = result;

            _.each(result, function(value, baseKey) {
                var splitted = baseKey.split('.'),
                length = splitted.length;

                if (length > 1) {
                    _.each(splitted, function (key, index) {
                        switch (true) {
                            case index < length - 1 && _.isUndefined(ref[key]):
                                ref[key] = {};
                                ref = ref[key];
                                break;
                            case index == length - 1:
                                ref[key] = value;
                                delete result[baseKey];
                                ref = result;
                                break;
                            default: 
                                ref = ref[key];
                                break;
                        }
                    });
                }
            });

            return result;
        }
    }
});